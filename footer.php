<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php get_template_part("/templates/template-parts/video-modal"); ?>
	<div class="footer-social-media bg-gold-gradient pt-xs pb-xs">
		<?php get_template_part("/templates/template-parts/social-media"); ?>
	</div>
	<footer id="colophon" class="site-footer">
		<div class="newsletter container center pt-md pb-sm"><?php get_template_part("/templates/template-parts/footer/newsletter"); ?></div>
		<div id="site-footer" class="clearfix">
			<div class="nav-footer"><?php get_template_part("/templates/template-parts/footer/nav-footer"); ?></div>
		</div>
		<div id="site-legal" class="pt-md pb-sm">
			<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
			<div class="site-terms"><a href=""><?php echo pll__('Legal Terms'); ?></a></div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
