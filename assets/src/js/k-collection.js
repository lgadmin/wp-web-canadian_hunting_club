var KCollection = (function(){
	var mobileBreakPoint = 480;
	var tabletBreakPoint = 768;
	var desktopBreakPoint = 1280;

	/**
	 * Reduce element height to 100vh - fixedItems' height. In order to cover exact one screen. 
	 * @param {Array} - Array of html elements' css query selector for fix positioned items on the screen
	 * @param {Dom Element} Target elements' css query selector for repositioning
	 *
	 * KCollection.RepositionFeatureItem(['#masthead'],'#home-feature-item', 1600);
	 */
	function RepositionFeatureItem(fixItemSelector, targetItemSelector, callback, tartgetItemMinWidth) {
	    var windowWidth = window.innerWidth;
	    var windowHeight = window.innerHeight;
	    var targetElements = document.querySelectorAll(targetItemSelector);

	    if(fixItemSelector.length > 0){
		    var fixItemHeightArray = fixItemSelector.map(function(querySelector){
		    	var items = document.querySelectorAll(querySelector);

		    	if(items.length > 0){
			    	var itemsHeightArray = Array.prototype.map.call(items,function(item){
			    		return item.offsetHeight;
			    	});
			    	var itemsHeight = Array.prototype.reduce.call(itemsHeightArray, function(total, single){
			    		return total + single;
			    	});
			    	return itemsHeight;
		    	}else{
		    		return 0;
		    	}
		    });

		    var fixItemHeight = fixItemHeightArray.reduce(function(total, single){
		    	return total + single;
		    });
		}else{
			var fixItemHeight = 0;
		}

	    // Append Parent to target element
	    Array.prototype.every.call(targetElements, function(item){
		    var parent = item.parentNode;
		    var wrapper;

		    if(parent.className == 'k-feature-item'){
		    	wrapper = parent;
		    }else{
				wrapper = document.createElement('div');

				wrapper.setAttribute('class', 'k-feature-item');
				// set the wrapper as child (instead of the element)
				parent.replaceChild(wrapper, item);
				// set element as child of wrapper
				wrapper.appendChild(item);
		    }

		    if(callback != null){
		    	callback();
		    }
	
	        var marginTop = (windowHeight - item.offsetHeight) / 2;
	        if (marginTop > 0) {
	            marginTop = 0;
	        }

	        if(item.offsetWidth < tartgetItemMinWidth){
	        	item.style.width = tartgetItemMinWidth;
	        }

	        wrapper.style.overflowX = 'hidden';
	        wrapper.style.overflowY = 'hidden';

	        if (item.offsetHeight + fixItemHeight < windowHeight) {
	        	wrapper.style.height = 'auto';
	        	item.style.marginTop = '0px';
	        } else if (windowWidth > tabletBreakPoint) {
	        	wrapper.style.height = (windowHeight - fixItemHeight) + 'px';
	        	item.style.marginTop = marginTop + 'px';
	        }
	    });
	}

   /**	 
	* Scroll header to element
	* Dependencies: jQuery
	* @param {Dom Element} - To element
	* @param {Object} Settings for animation, duration, etc
	* Usage
		var scrollTo = $(this).attr('href');
		var settings = {
	        duration: 2 * 1000,
	        offset: ($('.site-header').outerHeight()) * -1
	    };

		KCollection.headerScrollTo(scrollTo, settings);
	*/
	function headerScrollTo(toElement, settings){
		if(jQuery(toElement)[0]){
			var currentTop = jQuery(window).scrollTop();
			var elementTop = jQuery(toElement).offset().top;

			jQuery('html, body').stop().animate({
		        scrollTop: elementTop + settings.offset
		    }, settings.duration);
		}
	}

  return {
    RepositionFeatureItem: RepositionFeatureItem,
    headerScrollTo: headerScrollTo
  };
}());