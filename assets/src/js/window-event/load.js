// Windows Load Handler

(function($) {

    $(window).on('load', function(){

    	sticky_header_initialize();

      $('select').selectric();

    	//Top Feature Video
      if($(window).width() > 769){
          KCollection.RepositionFeatureItem(['#masthead'],'.top-feature-video', feature_video_play, 1600);  
      }else{
      }

      //Home page masonary
		  $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
      });

    });

}(jQuery));