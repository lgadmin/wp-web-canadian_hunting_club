// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('click', function(e){
          e.preventDefault();
        });

        
        if($(window).width() > 768){
          $('.dropdown-toggle').closest('li').on('mouseover', function(e){  
            if($(this).hasClass('dropdown')){
              $(this).addClass('opened');
            }
            $(this).find('.dropdown-menu').stop().slideDown('fast');
          });

          $('.dropdown-toggle').closest('li').on('mouseleave', function(e){
            if($(this).hasClass('opened')){
              $(this).removeClass('opened');
            }
            $(this).find('.dropdown-menu').stop().slideUp('fast');
          });
        }

        //Top feature video scroll
        $('.feature-video-scroll-down').on('click', function(){
          var scrollTo = 'main';
          var settings = {
                duration: 1000,
                offset: ($('#masthead').outerHeight()) * -1
            };

          KCollection.headerScrollTo(scrollTo, settings);
        });

        //PLL Language Switcher
        $('.pll-parent-menu-item > a').on('click', function(e){
          e.stopPropagation();
          $(this).siblings('.sub-menu').stop().slideToggle('fast');
          return false;
        });

        $('.pll-language-switcher .current-language a').on('click', function(e){
          e.stopPropagation();
          $('.pll-language-switcher .language-sub-menu').stop().slideToggle('fast');
          return false;
        });

        $(document).click(function() {
            $('.pll-parent-menu-item .sub-menu:visible').slideUp('fast');
            $('.pll-language-switcher .language-sub-menu').slideUp('fast');
        });

        // Home Page Grid Overlay
        if($(window).width() > 768){
          $('.grid-overlay').on('mouseover', function(){
            $(this).find('.overlay').addClass('hover-state').find('.content').stop().slideDown('fast');
          });

          $('.grid-overlay').on('mouseleave', function(){
            $(this).find('.overlay').removeClass('hover-state').find('.content').stop().slideUp('fast');
          });
        }
        
    });

}(jQuery));