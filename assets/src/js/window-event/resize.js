// Window Resize Handler

(function($) {

    $(window).on('resize', function(){

    	sticky_header_initialize();

    	//Top Feature Video
    	if($(window).width() > 769){
        	KCollection.RepositionFeatureItem(['#masthead'],'.top-feature-video', null, 1600);	
    	}

    })

}(jQuery));