var feature_video_play = function(){
    var myVideo = document.getElementById("feature_video"); 
    if(myVideo){
        console.log(myVideo);
        myVideo.play();
    }
}

function pauseVideos(){
	var videos = document.querySelector(".video");

    if(Array.isArray(videos)){
        Array.prototype.every.call(videos, function(vid){
    		vid.pause();
    	});  
    }else if(videos){
        videos.pause();
    }

    var videos = document.querySelector(".video-in-modal video");

    if(Array.isArray(videos)){
        Array.prototype.every.call(videos, function(vid){
            vid.pause();
        });  
    }else if(videos){
        videos.pause();
    }

}
// Windows Ready Handler

(function($) {

    $(document).ready(function(){

    	var video_modal = $('#video-modal-wrap');
    	var feature_video = $('#feature_video');

    	//Open video in modal
    	$('.modal-video').on('click', function(e){

    		var src = $(this).attr('href');

            e.preventDefault();

    		pauseVideos();

    		video_modal.find('.video-modal').addClass('playing').find('.video-in-modal video').attr('src', src).get(0).play();
            video_modal.fadeIn();

    	})

    	$('.modal-video-close').on('click', function(){
    		pauseVideos();
    		video_modal.fadeOut(function(){
    			feature_video_play();
    		});
    	});

    	//Video Modal inside
    	video_modal.find('.play-button').on('click', function(){
    		$(this).closest('.video-modal').addClass('playing').find('.video-in-modal video').get(0).play();
    	})

    	video_modal.find('.video-in-modal video').on('click', function(){
            $(this).get(0).pause();
    		$(this).closest('.video-modal').removeClass('playing').find('.play-button');
    	});
        
    });

}(jQuery));