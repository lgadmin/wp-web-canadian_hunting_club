<?php

//Custom Post Types
function create_post_type() {

    // Activity
    
    register_post_type( 'activity',
        array(
          'labels' => array(
            'name' => __( 'Activities' ),
            'singular_name' => __( 'Activity' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'chc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // Video
    
    register_post_type( 'video',
        array(
          'labels' => array(
            'name' => __( 'Videos' ),
            'singular_name' => __( 'Video' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'chc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>