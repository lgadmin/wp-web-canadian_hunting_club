<?php

function create_taxonomy(){

	register_taxonomy(
		'video-category',
		'video',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'video-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>