<?php

function lg_enqueue_styles_scripts() {
    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], wp_get_theme()->get('Version') );
	
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|IM+Fell+English+SC|PT+Serif:400,700', false );

	wp_enqueue_style( 'vendorCSS', get_stylesheet_directory_uri() . '/assets/dist/css/vendor.min.css', [], false );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), '1.0.1' );
	wp_register_script( 'vendorJS', get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js", array('jquery'), false );


	wp_enqueue_script( 'lg-script' );
	wp_enqueue_script( 'vendorJS' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function lg_dequeue_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );

?>