<?php

function add_template_support() {

	add_theme_support('post-thumbnails');

	add_theme_support('automatic-feed-links');

	add_theme_support( 'menus' );

	register_nav_menu( "top-nav", "Top Nav Menu(top-nav)" );
	register_nav_menu( "bottom-nav", "Bottom Nav Menu(bottom-nav)" );

	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

}

add_action('after_setup_theme','add_template_support', 16);


?>