<?php

# Help functions for the theme

// Format Phone Numbers Function
function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);
 
	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
	else
		return $phone;
}

// Format Phone Numbers Function
function format_phone_dot($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);
 
	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3", $phone);
	else
		return $phone;
}

function short_string($string, $limit){
	$chars = preg_replace( "/\r|\n/", " ", $string );
	$chars = explode(' ',strip_tags($chars));
	$chars = array_slice($chars, 0, $limit);

	return join(" ", $chars);
}

function isEnglish(){
	return get_locale() == 'en_CA';
}

function isChinese(){
	return get_locale() == 'zh_CN';
}

?>