<?php
/**
 * Template Name: Gallery
 */
?>

<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content template-gallery">

			<main>

				<!-- Gallery Section -->
				<div class="gallery">
				<?php
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'video',
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			            while( $result->have_posts() ) : $result->the_post(); 
			        	$thumbnail = get_field('thumbnail');
			        	$video = get_field('video');
			        ?>
			        	
			            <div>
				        	<a class="modal-video" href="<?php echo $video; ?>">
				        		<img src="<?php echo $thumbnail['url']; ?>">
				        		<div>
									<i class="fa fa-play-circle" aria-hidden="true"></i>
								</div>
				        	</a>
				        </div>

						<?php
			            endwhile;
			        endif; // End Loop

			        wp_reset_query();
				?>
				</div>
				<!-- end Gallery Section -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>