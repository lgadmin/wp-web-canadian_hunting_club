<?php
/**
 * Template Name: Membership
 */
?>

<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content membership">

			<!-- Top Feature Video -->
			<?php get_template_part("/templates/template-parts/page/top-feature-video"); ?>
			<!-- end Top Feature Video -->

			<main>

			<!-- Intro Section -->
			<?php
				$memberships_background_image = get_field('memberships_background_image');
			?>
			
			<div class="pt-lg pb-lg center membership-list" style="background-image: url('<?php echo $memberships_background_image; ?>')">
				<div class="container">
					<?php
						$args = array(
				            'showposts'	=> 3,
				            'post_type'		=> 'product',
				            'tax_query' => array(
								array(
									'taxonomy' => 'product_cat',
									'field'    => 'slug',
									'terms'    => array('membership')
								),
							),
				        );
				        $result = new WP_Query( $args );

				        // Loop
				        if ( $result->have_posts() ) :
				            while( $result->have_posts() ) : $result->the_post(); ?>
				        	
				            <div>
				            	<h2><?php the_title(); ?><hr></h2>
				            	<?php the_content(); ?>
				            </div>

							<?php
				            endwhile;
				        endif; // End Loop

				        wp_reset_query();
					?>
				</div>
			</div>
			<!-- end Intro Section -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>