<?php
/**
 * Template Name: Grid Template
 */
?>

<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<!-- Top Feature Video -->
			<?php get_template_part("/templates/template-parts/page/top-feature-video"); ?>
			<!-- end Top Feature Video -->

			<main>

			<!-- Grid Box Main -->
			<?php get_template_part("/templates/template-parts/page/grid-box"); ?>
			<!-- end Grid Box Main -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>