<?php
/**
 * Template Name: Contact
 */
?>

<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content contact">

			<!-- Top Feature Video -->
			<?php get_template_part("/templates/template-parts/page/top-feature-video"); ?>
			<!-- end Top Feature Video -->

			<main>

			<!-- Grid Box Main -->
			<?php 
				$form_title = get_field('form_title');
				$form_copy = get_field('form_copy');
				$form_background_image = get_field('form_background_image');
			?>
			<div class="pt-lg pb-lg center g-form" style="background-image: url('<?php echo $form_background_image; ?>');">
				<div class="g-form">
					<h2 class="h2 uppercase"><?php echo $form_title; ?></h2>
					<?php echo $form_copy; ?>
					<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
				</div>
			</div>
			<!-- end Grid Box Main -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>