<?php
/**
 * Template Name: Activity
 */
?>

<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content activity">

			<!-- Top Feature Video -->
			<?php get_template_part("/templates/template-parts/page/top-feature-video"); ?>
			<!-- end Top Feature Video -->

			<main>

			<!-- Intro Section -->
			<?php
				$intro_title = get_field('intro_title');
				$intro_copy = get_field('intro_copy');
				$intro_background = get_field('intro_background');
			?>
			<div class="activity-intro" style="background-image: url('<?php echo $intro_background; ?>');">
				<div class="content center">
					<h2 class="h2"><?php echo $intro_title; ?><hr></h2>
					
					<?php echo $intro_copy; ?>
				</div>
			</div>
			<!-- end Intro Section -->

			<!-- Gallery Section -->
			<?php
			if( have_rows('gallery') ):
				?>
				<div class="pt-xs pb-xs center heading bg-gold-gradient">Gallery</div>
				<div class="gallery">
				<?php
			    while ( have_rows('gallery') ) : the_row();
			        $thumbnail = get_sub_field('thumbnail');
			        $video = get_sub_field('video');
			        ?>
			        <div>
			        	<a class="modal-video" href="<?php echo $video; ?>">
			        		<img src="<?php echo $thumbnail['url']; ?>">
			        		<div>
								<i class="fa fa-play-circle" aria-hidden="true"></i>
							</div>
			        	</a>
			        </div>
			        <?php
			    endwhile;
			    ?>
			    </div>
			    <?php
			else :
			    // no rows found
			endif;

			?>
			<!-- end Gallery Section -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>