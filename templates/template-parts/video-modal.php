<div id="video-modal-wrap">
	<div class="video-modal">
		<div class="video-in-modal">
			<video src="" playsinline muted loop></video>
		</div>
		<div class="play-button">
			<i class="fa fa-play-circle" aria-hidden="true"></i>
		</div>
		<a class="modal-video-close"><i class="fa fa-times" aria-hidden="true"></i></a>
	</div>
</div>