<?php

	$translations = pll_the_languages(array('raw'=>1));

?>

	<div class="pll-language-switcher">
		<div class="current-language">
			<?php foreach ($translations as $key => $translation): ?>
				<?php if($translation['current_lang'] == true): ?>
					<a href="javascript:void(0);"><?php echo $translation['name']; ?></a>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<div class="language-sub-menu">
			<?php foreach ($translations as $key => $translation): ?>
				<a href="<?php echo $translation['url']; ?>"><?php echo $translation['name']; ?></a>
			<?php endforeach; ?>
		</div>
	</div>