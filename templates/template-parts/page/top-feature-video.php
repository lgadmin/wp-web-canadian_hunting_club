<?php
	$video_src = get_field('feature_video')['video_src'];
	$video_content = get_field('feature_video')['video_content'];
	$second_video_src = get_field('feature_video')['second_video_src'];
?>

<div class="top-feature">
	<?php if($video_src): ?>
	<div class="top-feature-video">
		<video id="feature_video" class="video" loop autoplay playsinline muted>
		  <source src="<?php the_field('video', $video_src); ?>" type="video/mp4">
		Your browser does not support the video tag.
		</video>

		<div class="overlay">
			<?php if(!is_front_page()): ?>
				<div class="page-title"><?php the_title(); ?></div>
			<?php endif; ?>
			<?php echo $video_content; ?>
			<?php if(is_front_page()): ?>
				<?php get_template_part("/templates/template-parts/pll-language-switcher-dropdown"); ?>
			<?php endif; ?>
			
			<a class="feature-video-scroll-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
		</div>

		<?php if($second_video_src): ?>
		<div class="second-video">
			<a href="<?php the_field('video', $second_video_src); ?>" class="modal-video">
				<div>
					<i class="fa fa-play-circle" aria-hidden="true"></i>
				</div>
			</a>
		</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
</div>