<?php
	if( have_rows('grid_items') ):
		?>
		<div class="grid">
		<div class="grid-sizer"></div>
		<?php
	    while ( have_rows('grid_items') ) : the_row();
	        if( get_row_layout() == 'image_section' ):
	        	$width = get_sub_field('width');
		        $height = get_sub_field('height');
		        $thumbnail = get_sub_field('thumbnail');
		        $video = get_sub_field('video');
		        $link = get_sub_field('link');
		        $title = get_sub_field('title');
		        $content = get_sub_field('content');
	        	?>
					<div class="grid-item grid-overlay grid-item--width<?php echo $width; ?> grid-item--height<?php echo $height; ?>" style="background-image: url('<?php echo $thumbnail['url']; ?>');">
						<?php if($video): ?>
						<a href="<?php the_field('video', $video); ?>" class="modal-video visible-xs">
							<div>
								<i class="fa fa-play-circle" aria-hidden="true"></i>
							</div>
						</a>
						<?php endif; ?>
					  	<div class="overlay">
					  		<div class="title">
					  			<?php if($link): ?><a href="<?php echo $link; ?>"><?php endif; ?><h2 class="h2"><?php echo $title; ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></h2> <?php if($link): ?></a><?php endif; ?>
					  			
					  		</div>
					  		<hr>
					  		<div class="content">
					  			<?php echo $content; ?>
					  			<?php if($video): ?>
					  			<a href="<?php the_field('video', $video); ?>" class="cta cta-gold-hover modal-video"><?php echo pll__('WATCH THE VIDEO'); ?></a>
					  			<?php endif; ?>
					  			<?php if($link): ?>
								<a href="<?php echo $link; ?>" class="cta cta-gold-hover"><?php echo pll__('LEARN MORE'); ?></a>
					  			<?php endif; ?>
					  		</div>
					  	</div>
					</div>
	        	<?php
	        elseif( get_row_layout() == 'text_section' ):
	        	$width = get_sub_field('width'); 
		        $height = get_sub_field('height'); 
		        $image = get_sub_field('image'); 
		        $content = get_sub_field('content'); 
		        $social_media = get_sub_field('social_media');
	        	?>
					<div class="grid-item center grid-item--width<?php echo $width; ?> grid-item--height<?php echo $height; ?>" style="background-image: url('<?php echo $image; ?>');">
						<?php echo $content; ?>
						<?php if($social_media == 1): ?>
						<div class="pt-xs">
							<?php get_template_part("/templates/template-parts/social-media"); ?>
						</div>
						<?php endif; ?>
					</div>
	        	<?php
	        endif;
	    endwhile;
	    ?>
		</div>
	    <?php
	else :
	    // no layouts found
	endif;
?>